[TOC]

# Failures assessment

This page outlines various challenges encountered during the development of the Helmholtz Cage and details the actions we've undertaken to investigate and resolve these issues.

## 1. PCB failure assessment

### Problem statement and description

At the early stages of PCB debugging one H-bridge ICgot burned.

The PCB was tested without the load, the wiring, and switch and worked correctly.

Afterwards, after connecting everything with the CB, a voltage drop was observed.

The cause was the switch, which was overheated. And caused a voltage drop from 47.5 to approx. ~41V.
However, the DRV8871DDA voltage was dropped to 2-4 V while operating at 100% duty cycle. (With no load connected it outputted the input voltage of approx. 48V).

The exact resistance of the final coils setup with the wiring is described in the issue #12.(approx. ~38-50Ohm)

After disconnecting the switch and bypassing it a spike occurred and caused the DRV8871DDA of the first pair of coils to get burnt.(PCB schematic reference INx_0, H-bridge).



<p align="center">
  <img src="uploads/damaged-PCB.png" width="400" />
</p>


Specifically, the spike occured on the pad 5 of the DRV8871DDA:

<p align="center">
  <img src="uploads/pcb-burn.png" width="400" height = "300"/>
  <img src="uploads/zoomed-pcb.png" width="400" height = "300"/>
</p>


The other identical circuits for DRV8871DDA IC remained unaffected, though connected to the same pad 'ON/OFF system', which supplied the 48V. Maximum current draw per IC should have been approx. ~1.2A.


### Failure assessment steps

- Check datasheet specifications(maximum current, voltage, and temperatures)
- Remove the burnt IC and measure the temperature rise for the PCB components with load
- Double-check the harnesses, and all of the connection
- Double-check the schematic for connection errors
- Investigate the theory of the coils inductance causing a voltage spike, due to unexpected change in current flow, creating a "back EMF" possibly damaging the IC

### Thermal assessment

First of all, we measured the DRV8871DDA IC with 100% duty cycle outputting approx. 47V to the coils. The temperature seemed fine. Actually, according to the datasheet p. 16: "If the die temperature exceeds approximately 175°C, the device is disabled until the temperature drops below the temperature hysteresis level"

> The die temperature refers to the internal operating temperature of the DRV8871-Q1 motor driver IC itself

Thus, is highly unlikely that overheating caused this failure. However, we did not measure the exact IC that got burnt of course. The load resistance is different for the 3 circuits. Specifically, the circuit that got burnt was connected to a load of 38.3 Ohm. While the one that was measured was connected to 50.2 Ohm.

Temperature measurement of DRV8871DDA with load (3rd pair of coils/Schematic ref:H-bridge2) is shown in the picture below:



Though, we noticed a very large temperature rise in the MCU of our NUCLEO board. Specifically:


<p align="center">
  <img src="uploads/thermal2.png" width="400" />
  <img src="uploads/thermal3.png" width="400" />
</p>

The left figure depictures a NUCLEO MCU operating connected to the coils PCB system. While the right shows NUCLEO MCU connected only to a laptop for charging reasons.

These temperatures are highly elevated, pointing out to a potential problem.

For reference, here is another NUCLEO board, that has never been connected to the PCB board, charging from a laptop(30-60°C). 


Temperature measurement of an uncompromis NUCLEO board:

<p align="center">
  <img src="uploads/thermal4.png" width="400" />
</p>


### Inductive Load Kickback Τheory

We use a DRV8871DDA H-bridge. This IC has internal flyback diodes to prevent inductive current spikes from the coils. Thus, this theory is unlikely, unless the diodes cannot handle the inductance of the specific setup.

<p align="center">
  <img src="uploads/flyback.png" width="400" />
</p>

A calculation of the coil's inductance is shown in the picture below, while the actual inductance will be double that (so around 24mH) because we have 2 coils in series:


<p align="center">
  <img src="uploads/induct.png" width="300" />
</p>


### Conclusion

Upon further investigation we discovered that the switch was improperly connected, due to the fact that it was wrongly marked by the manufacturer. Instead of relying on the contour-intuitive marking, one should have measured the continuity. Thus, the cause of the IC's burning up was discovered. It was overheating and after disconnecting it and bypassing it, a large spike was caused and burnt an IC driver circuit. This circuit could have backfired and caused some component damage on the NUCLEO that now causes its overheating, since its MCU is overheating even when disconnected from the Helmholtz Cage PCB.

## 2. Assessing Issues of Incorrect Output Voltage

### Problem statement and description

The board worked properly without the coils connected, outputting the right PWM signal. Though, while the coils were connected you could hear a high-pitched buzzing sound and the output voltage was approx. ~2V with 50% duty cycle(it should be ~24V).
Sometimes while re-flushing code and only one pair of coils was connected you could measure a PWM output of ~48V with a duty cycle of 50%, which was not correct and circumstantial.

### Conclusion

Due to the coils' inductance the PWM signal was altered while using only the driving mode. As a consequence, we observed non-desirable results. Instead, when using PWM, it typically works best to switch between driving and braking. For example, to supply the coils with forward current we should have used IN1 = 1 and IN2 = 0 during the driving period, and IN1 = 1 and IN2 = 1 during the other period, as described in the ([datasheet p. 8](https://www.ti.com/lit/ds/symlink/drv8871.pdf?HQS=dis-mous-null-mousermode-dsf-pf-null-wwe&ts=1690789480293&ref_url=https%253A%252F%252Fgr.mouser.com%252F)).

Below figures of oscilloscope reading the PWM signals are presented.

<p align="center">
  <img src="uploads/oscilloscope1.png" width="400"  height="300"/>
  <img src="uploads/oscilloscope2.png" width="400"  height="300"/>
</p>


Specifically, the picture picture shows PWM signals while using only driving mode, and the right PWM signal while using driving and braking mode.

## 3. Channel 2 malfunctioning assesment


### Problem statement and description

After successfully performing the duty cycle mapping to the magnetic field on channels 1 and 3, I noticed that the second channel did not behave as intended. The magnetic field generated by this channel were insufficient. Thus, I proceeded to further measurements. The output PWM voltage of the H-bridge, applied to the pair of coils connected to channel 2 was measured. Regardless of the duty cycle, the measurement was either 0 or 48V (equal to the system's input voltage).

### Resolution steps

Thus, I proceeded with an oscilloscope to measure different outputs. I started from removing the coils system from the control box and made the following observations:
1. First of all, the input voltage was within range(~48V). 
2. The STM32 signal PWM output was generated correctly.
3. In an effort to see if the second IC had been damaged by the spike that burned the first H-bridge, I replaced it. The same behavior was noticed, though.
4. Thus, I proceeded to measuring the input PWM channels to the IC. I observed no signal on one of the pins. Subsequently, I figured out that, one of the channels was connected to the wrong NUCLEO pin on the PCB.
5. Thus one issue was discovered: the footprint of the NUCLEO was incorrect.

Following that, some physical PCB modifications were made. Some of the solder-mask on the incorrectly connected trace was removed, disconnecting it from the mistaken pin. Following that, a wire was added to connect it to the correct one. The wire was then secured with hot glue to prevent short circuits with neighboring pins and traces on the PCB. Following that, continuity testing was performed, and the system was finally powered on and tested to ensure that it was producing the correct voltage.

Finally, one problem can be considered resolved.

### Further observations

- While the supplied voltage is correct, as expected from the duty cycle, the magnetic field produced by channel 2 is insufficient. It has a magnitude of 10uT, whereas the other coils have a magnitude of 100uT or higher. 
- As I measured the anticipated resistance on the coils, one can conclude that there are no open or short circuits. 
- The magnetometer appears to be operational as well, since I measured the magnetic field with another device.
- The measured current should have produced a magnetic field with a magnitude of 50uT or greater. 
- The same behavior is observed when the coil is plugged into the other channels, which work properly with the other coil sets. 

These lead us to believe that the second coils pair setup has some issues, probably related to physical phenomena we did not account for.

### Conclusion


Finally, we determined that the problem was related to the direction of the current in the half-coils, as if they were electrically connected in series opposition.

Actually, we unintentionally developed a distinct configuration known as an anti-Helmholtz coil. The geometry is the same, but the magnetic fields from each half-coil are subtracted. As a result, the magnetic field distribution is positive near one half-coil, zero in the middle, and negative towards the other half-coil.

<p align="center">
  <img src="uploads/anti-helmholtz_coil.png" width="400"/>
</p>


## 4. Assesing noise due to PWM power signals

### Problem statement and description

According to spice simulation, the rise time and the fall time of the PWM power signal seams to be a problem that can cause noise in the generated magnetic field.  For, example a 1us rise time produces approx. 40mA ripple, while a 0.1us rise time produces 100mA ripple! (For reference, in logic signals, the rise times are close to the order of ns).

The step to investigate the issue were the following:
- Measure magnetometer noise with and without the power PWM signal present
- Measure the rise time with an oscilloscope
- If the noise due to PWM power signals is significant, add a capacitor to affect the rise/fall time


### Noise measurements

#### Noise measurements without powering the cage setup

| Statistical data for X axis magnetometer measurements |   Value         |   Unit |
|----------------------------------------------------------------------------------------|------------|----|
| The mean                                                                               | -32.397153 | uT |
| The standard deviation                                                                 | 0.035882   | uT |
| The variance                                                                           | 0.001288   | uT^2 |

| Statistical data for Y axis magnetometer measurements |   Value       |  Unit    |
|----------------------------------------------------------------------------------------|----------|------|
| The mean                                                                               | 7.381032 | uT   |
| The standard deviation                                                                 | 0.088777 | uT   |
| The variance                                                                           | 0.007881 | uT^2 |

| Statistical data for Z axis magnetometer measurements |    Value       |  Unit    |
|----------------------------------------------------------------------------------------|-----------|------|
| The mean                                                                               | 35.484191 | uT   |
| The standard deviation                                                                 | 0.306350  | uT   |
| The variance                                                                           | 0.093851  | uT^2 |

#### Noise measurements while supplying MAX current to the cage setup using 500 Hz PWM

| Statistical data for X axis magnetometer measurements |     Value        |  Unit    |
|-------------------------------------------------------|-------------|------|
| The mean                                              | -197.929721 | uT   |
| The standard deviation                                | 0.325000    | uT   |
| The variance                                          | 0.105625    | uT^2 |

| Statistical data for Y axis magnetometer measurements |    Value       | Unit     |
|-------------------------------------------------------|-----------|------|
| The mean                                              | 47.171570 | uT   |
| The standard deviation                                | 0.636264  | uT   |
| The variance                                          | 0.404832  | uT^2 |

| Statistical data for Z axis magnetometer measurements |    Value         | Unit     |
|-------------------------------------------------------|-------------|------|
| The mean                                              | -113.984691 | uT   |
| The standard deviation                                | 0.642243    | uT   |
| The variance                                          | 0.412476    | uT^2 |



#### Noise measurements while supplying MAX current to the cage setup using 5 kHz PWM

| Statistical data for X axis magnetometer measurements |             |      |
|-------------------------------------------------------|-------------|------|
| The mean                                              | -201.691765 | uT   |
| The standard deviation                                | 0.128429    | uT   |
| The variance                                          | 0.016494    | uT^2 |

| Statistical data for Y axis magnetometer measurements |           |      |
|-------------------------------------------------------|-----------|------|
| The mean                                              | 62.984654 | uT   |
| The standard deviation                                | 0.208089  | uT   |
| The variance                                          | 0.043301  | uT^2 |

| Statistical data for Y axis magnetometer measurements |             |      |
|-------------------------------------------------------|-------------|------|
| The mean                                              | -114.709073 | uT   |
| The standard deviation                                | 0.382588    | uT   |
| The variance                                          | 0.146374    | uT^2 |

### Rise/Fall time measurements
The rise time, according to `RIGOL DS1102E` oscilloscope, is approx. 182 ns.

The fall time is oscillating between 90 and 200 ns. The oscillation is probably caused by the mode switching from driving to breaking.

The measurements were taking for a 50% Duty Cycle, 5kHz signal.

<p align="center">
  <img src="uploads/rise-time.png" width="280" height = "220"/>
  <img src="uploads/fall-time1.png" width="280" height = "220"/>
  <img src="uploads/fall-time2.png" width="280" height = "220"/>
</p>



### Conclusion

We determined that the noise due to PWM power signals is not too significant, and an addition of a capacitor to affect the rise/fall time is not needed, since our requirements are met.

## 5. Channels' current dependence on the cumulative power consumption

### Problem statement and description

The existing reliance on external channels was detected through the utilization of a digital multi-meter, specifically the RIGOL DM3058 model. I hold the presumption that the accuracy of the measurement is compromised due to the utilization of a pulsed current signal at a frequency of 5kHz. To gain a more accurate understanding of the situation, I intend to conduct current measurements using the RIGOL DS4024 oscilloscope.

### Use of oscilloscope for current measurements

#### Introduction

An oscilloscope serves as a tool to observe the voltage variation between two points, enabling the graphical representation of the signal over time.

For determining the current flowing through a circuit, a widely used approach involves the utilization of a shunt resistor. This technique entails placing a low-value resistor in line with the power or return line. By measuring the voltage across this resistor, Ohm's Law $I = V/R)$ can be employed to calculate the current entering the load. This calculated current aligns with the current flowing throughout the circuit at that particular moment.

Typical shunt resistor values, denoted as $R_{sh}$, fall within the range of 0.01 to 1 Ω. Opting for higher $R_{sh}$ values enhances measurement precision, albeit with the trade-off of increased voltage drop in the power supply route to the load.

#### Shunt Resistor Power Considerations

An important consideration is the power dissipation of the shunt resistor. As current draw increases, the resistor's heat dissipation also rises, posing a risk of damage or even fire. We will implement a 1 Ohm resistor with a power rating of 1/4W. Consequently, the maximum safe measurable current can be determined using the equation:

```math
P = I_{max}^2 R_{sh} \rightarrow I_{max} = 0.5A
```

The average current that we measure can be effectively affected by the duty cycle of the H-bridge we are using on each power line. 

#### Mesuring setup


##### High-side measurement 
To establish an oscilloscope configuration for current measurement, we consider the arrangement employing the resistor on the positive rail, known as a high-side shunt resistor. Nevertheless, an issue emerges due to the direct connection of the ground clip on typical desktop oscilloscopes to earth ground. This situation poses a challenge: when working with both a grounded circuit and a correctly grounded desktop oscilloscope, connecting the ground clip to either side of Rsh would generate a short circuit, resulting in a highly undesirable outcome.

##### Low-side measurement 
An alternative approach involves relocating the resistor to the return path, referred to as a low-side shunt resistor, and connecting the oscilloscope's ground clip to the circuit's ground. This adjustment eliminates the risk of shorting out the power supply. However, this modification introduces a new concern: the emergence of a ground loop. Within this loop, current can circulate from earth ground through the circuit under examination, through the oscilloscope's ground clip, and then back to ground through the oscilloscope. Ground loops have the potential to introduce unwanted interference or noise into measurements or the circuit itself. Notably, this issue arises only when both the oscilloscope and the circuit under test are connected to earth ground, as is the case in our current setup.

##### Differential measurement

To conduct this measurement, two channels on the oscilloscope are required. Most oscilloscopes typically have interconnected ground clips (this can be verified using a multimeter if uncertain). Consequently, there is no need to establish any connections for the ground clips.

On the oscilloscope interface, access the Math function. From this menu, you can graphically depict the output of Channel 1 minus Channel 2. By subtracting the voltage of Channel 2 from that of Channel 1, the voltage drop across the resistor can be accurately calculated, all while avoiding concerns about power supply shorting or ground loop formation.

### Current measurements

Upon noticing the anomalous and implausible nature of the measurements conducted by using the RIGOL DM3058 digital multi-meter, a decision was made to conduct a re-evaluation of the entire measurement process. This time, a more precise and reliable measurement tool was used to ensure the accuracy and validity of the recorded data.

The measurements were taken employing the RIGOL DM3058 multi-meter. The obtained measurements exhibited a higher degree of reliability when contrasted with the previously outlined measurements conducted utilizing an unreliable handheld multi-meter. It is imperative to note that all channels were operated under a maximum duty cycle of 100%. These channels were powered by a 48VDC [USP-225-48](https://www.sager.com/_resources/pdfs/product/USP-225.pdf) power supply unit.

The subsequent sections present the results derived from the current measurements across the three utilized channels:


#### Channel 1 current measurements

| Connected channel/s | Measured current  |
|---------------------|-------------------|
| 1                   | 1.310 A           |
| 1 and 3             | 1.145 A           |
| 1 and 2             | 1.310 A           |
| 1,2, and 3          | 1.138 A           |

#### Channel 2 current measurements
| Connected channel/s | Measured current  |
|---------------------|-------------------|
| 2                   | 0.985 A           |
| 2 and 3             | 0.963 A           |
| 2 and 1             | 0.966 A           |
| 1,2, and 3          | 0.960 A           |

#### Channel 3 current measurements

| Connected channel/s | Measured current  |
|---------------------|-------------------|
| 3                   | 1.438 A           |
| 3 and 2             | 1.430 A           |
| 3 and 1             | 1.207 A           |
| 1,2, and 3          | 1.194 A           |

### Conclusion

In conclusion, the findings indicate that there exists a discernible dependence of the current on the cumulative current consumption of the other channels. This phenomenon can be attributed to the observed voltage drop across the coil ends. This correlation could potentially arise due to the incorporation of additional loads in parallel, leading to slight adjustments in the power supply's output voltage to accommodate the added load impedance. Consequently, this adaptive adjustment could facilitate a more optimal distribution of current among the connected loads, ultimately resulting in a reduction in the total current draw.

Upon closer examination, the extent of this dependence is not as dire as initially implied by the faulty multi-meter readings. However, it still falls short of meeting our accuracy requirements. Notably, the changes in current attributed to variations in other channels surpass 70mA, which approximately corresponds to a magnetic field fluctuation of 10 microtesla (uT). This magnitude exceeds our established accuracy threshold for magnetic field fluctuations.

It is worth noting that operating with lower duty cycles, thereby reducing the current drawn from each channel, mitigates this reliance on the concurrent functioning of other channels.

As a result of these findings, the development of a Helmholtz Cage calibration procedure that incorporates these interdependencies to align with specifications is imperative. Moreover, it is crucial to address the fact that the magnetic field fluctuations are not solely a consequence of current dependency on other channels. They also arise from the non-ideal orthogonality of the cage setup, inducing unintended magnetic fields generation in other orthogonal axes.



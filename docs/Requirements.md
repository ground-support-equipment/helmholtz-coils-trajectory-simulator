[TOC]

This project consists of a design of a 3D Helmholtz cage. The cage will be utilized
to test a satellite’s attitude determination and control system, by recreating on-
orbit magnetic fields in a realistic manner on the ground. The system will be
sized to meet the requirements of our next “Phasma” mission, however it will
also be capable of supporting subsequent missions.

# Goals
With the following project, we should be able to accomplish the following:

 - The capacity to generate a consistent magnetic field in a compartment
   large enough to house a 3U CubeSat plus a few deployable structures
 - The capacity to simulate dynamic magnetic fields such as those
   observed in low-Earth orbit (LEO) 
 - The ability to cancel out the ambient magnetic field in our laboratory’s location(hackerspace.gr), which is equal to 0.46 Gauss(approx.)
 - The simplicity of use and serial communication with the magnetic field
controller
 - The development of documentation to facilitate future
system installs by users who are unfamiliar with the system
 - The documentation of the design process and research done
 - The easiness of assembly, storage, and disassembly
In addition to providing immediate results needed for upcoming projects
(e.g. PHASMA), this project will provide open knowledge for other satellite
teams and constitute a basis for further ADCS development in other LSF
missions.


# Requirements

|                Requirement               	|       Value      	|
|:----------------------------------------:	|:----------------:	|
| Volume of the constant filed zone        	| 64L (40x40x40cm) 	|
| Output magnetic field range              	|   -1.75 to 1.75 Gauss  	|
| Input voltage (per coil)           	|    48V    	|
| Max. input current (per coil)            	|       1.2A          	|
| Pair of Coils Series Resistance  	|       45Ω      	|
| Time of assembly and disassembly         	| < 1hr 	|
| Maximum variation of the field           	|      < 10µT      	|


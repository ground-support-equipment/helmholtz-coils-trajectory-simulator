# Helmholtz cage mechanical design

Helmholtz coils mechanical design is based on the following [project](https://charleslabs.fr/fr/project-ECE3SAT+-+Helmholtz+Coils).

The specifications of the design are the following:

  * Square of 0.8(0.796)x0.8(0.796)m
  * Spacing between of squares): 0.4m
  * U-shaped aluminum profile 10x10mm. Needed aluminum profile 16x780mm and 8x800mm, \~20m
  * Turns: 80 of 24AWG or 0.5mm (needed coil wire: 4\*0.8(0.796)\*80 \~256m (x6))
> Shops in Athens for coil wire:
> 
>  * [https://stefos-xalkos.gr/](https://stefos-xalkos.gr/)
>  * [http://www.sinadinos.gr/site_pages/sinadinos_Home_gr.html](http://www.sinadinos.gr/site_pages/sinadinos_Home_gr.html)

The CAD design and the actual final model are depicted below:

<p align="center">
  <img src="uploads/fdf4bed039795297717e343e14889eb3/coils.png" width="400" height = "310" />
  <img src="uploads/White_Background_mech_setup.png" width="400" height = "310" /> 
</p>


Significant improvements are required in the mechanical setup for several reasons. Firstly, it lacks reliability due to the vulnerability of 3D printed components, which are prone to damage or breakage during installation. Excessive use of tape for securing components is not ideal for disassembly/re-assembly purposes.

Secondly, a crucial error was made in the dimensions, as each rectangle should have been slightly larger to accommodate the one fitted inside it. This mismatch led to bulging and fitting issues, particularly with four pairs of 82x84mm and two pairs of 84x84mm components. This compromises the overall structural integrity.

Furthermore, the setup lacks orthogonality and sturdiness, making it susceptible to easy movement. This is a critical concern, especially considering the need for lengthy calibration procedures. Any unintended movement of the setup could introduce errors, necessitating repeated calibration procedures.


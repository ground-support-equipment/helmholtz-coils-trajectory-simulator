[TOC]

# Electronics Design Components

The Helmholtz Cage electronics setup consist of the following componets:

1. Magnetometer
2. Power Supply
3. Micro-controller
4. Current Sensor
5. H-bridge

The following sections will examine each parts's design in detail.

## 1. Magnetometer


### Model Selection

The RM3100 Magnetic Sensor has been chosen for use in this project, and the testing board developed by the Positioning Navigation Intelligence (PNI) Corps is utilized. The datasheet and user manual are available on their [website](https://www.pnicorp.com/rm3100/). The selection of this sensor was based on its availability in the lab and its exceptional accuracy and low noise provided by its 3-axis magnetometer. This is essential for our Helmholtz Cage project, as it requires a uniform magnetic field with precise measurements of the magnetic field. Additionally, the sensor has a wide dynamic range that enables it to accurately detect both strong and weak magnetic fields. Most simple COTS magnetometer modules do not offer the desired range (-175 uT to 175 uT) or the required accuracy for this project.


### Firmware
A custom C driver was developed to interface the RM3100 magnetometer sensor with the STM32 microcontroller. The driver includes initialization functions and low-level communication through the I2C communication protocol to acquire data from the sensor. The `RM3100_Initialize()` function is responsible for initializing the sensor by setting the parameters in the struct and configuring the registers. In our case, the Cycle Count, Continuous Measurement Mode and Rate, and DRDY Status registers were configured. Additionally, the `RM3100_ReadMagneticField()` function reads the magnetic field data from the sensor and applies calibration to the raw readings using the RM_SCALE and RM_OFFSET arrays, which are determined by the magnetometer calibration procedure outlined [here](https://gitlab.com/librespacefoundation/helmholtz-coils-sw/-/blob/main/Documentation/Magnetometer-Calibration.md?ref_type=heads). The aforementioned firmware for the RM3100 sensor can be found in the table below:

| Resource   | Link                                                                                          |
|--------|-----------------------------------------------------------------------------------------------|
| Source file | https://gitlab.com/vi2000/helmholtz-coils-sw/-/blob/main/firmware/Core/Src/RM3100.c?ref_type=heads |
| Header file | https://gitlab.com/vi2000/helmholtz-coils-sw/-/blob/main/firmware/Core/Inc/RM3100.h?ref_type=heads |
| Usage Example | https://gitlab.com/vi2000/helmholtz-coils-sw/-/blob/main/firmware/Core/Src/main.c?ref_type=heads#L803 |


### Harnesses and mounting

A perfboard was utilized to connect the magnetometer to the microcontroller (a PCB wasn't made just to speed up the development of the project). This perfboard manages all the necessary connections from the RM3100 testing board to the output connectors. The output connectors include a JST XH 2-pin for 3.3V and GND pins and a 3-pin JST XH for SDA, SDL, and DRY pins. To account for the distance between the magnetometer and the micro-controller housed in a control box, long harnesses were created in order to be able to place the magnetometer at the center of the Cage.


<p align="center">
  <img src="uploads/d31596c18afbe5c36e9c114da6aee3d7/image.png" width="400" />
</p>

Additionally, a custom mechanical modular base was constructed to mount the perfboard and level the magnetometer at the center of the cage. It consists of PETG printed parts and Round Acetal Tubes. 


<p align="center">
  <img src="uploads/924670682ec89908d38df7c733acb56f/image.png" width="400" />
</p>


## 2. Power supply

The coils should be powered by a power supply that can provide enough voltage and current to generate the desired magnetic field strength and range. A DC power supply was opted for and H-bridges were utilized to vary and reverse the output current according to the input from the software that controls the cage.

The simulation results showed that ~40 V are necessary to achieve a magnetic field range of ±1.5 Gauss.

The [USP-225-48](https://www.sager.com/_resources/pdfs/product/USP-225.pdf) power supply was used as the final choice. It has a universal full range AC input. The DC voltage output is 48V and the current rating is 4.7A. The magnetic field that can be generated is within the range of ±1.72 Gauss. The earth’s magnetic field vector's magnitude is about 0.4 Gauss. Therefore, the minimum magnetic field that is needed is ±0.8G/±0.1G to cancel out the earth’s magnetic field and create an opposite one. However, a system that can generate a higher magnetic field of ±1.72 Gauss was designed to offer more flexibility in the future.

The power supply wiring was prepared and tested successfully, outputting \~47.7 V. Furthermore, we contained the power supply in an enclosure for safety reasons.


<p align="center">
  <img src="uploads/9aaac3fcb4dab7aec5e285f73786e4ef/image.png" width="400" />
</p>


## 3. Microcontroller

### Requirements

* PWMs to drive the H-bridges (at least 6)
* SPI or I2C to read magnetometer
* 3 ADCs for current sensing
* UART as user interface
* Optional GPIO pins to enable/disable H-bridges

The original plan was to apply the magnetic field values obtained through UART communication using an Arduino UNO. However, in order to be able to convert the TLE information (a satellite trajectory) into a magnetic field, a more powerful microcontroller, such as the STM32, or microprocessor, such as the Raspberry Pi, must be used. Thus, we opted for the use of a **NUCLEO-L476RG** board to control the coils. The user will only need to supply a TLE for the investigated satellite; the microcontroller will handle the rest.

Due to several difficulties, such as the implementation of Neural Network to convert the magnetic field to duty cycle values and the TLE conversion to magnetic field, which could possibly be done in C but would require a greater development time, we use the STM32 micro-controller exclusively for imposing duty cycle to the H-bridges and reading magnetometer data. Everything else is handled by Python. However, the selection of the STM32 board offers the possibility of expanding the microcontroller's potential uses in the future.

## 4. Current Sensing

We wanted to include a current sensor in order to be able to design a current feedback loop. We investigated the following options:

1. Shunt Current Sensor [INA169 Analog DC Current Sensor Breakout - 60V 5A Max](https://learn.sparkfun.com/tutorials/ina169-breakout-board-hookup-guide/all)
2. Hall Effect Sensor [ACS724/5](https://www.allegromicro.com/~/media/files/datasheets/acs723kma-datasheet.pdf)

Starting from evaluating the first option we discovered that in order to achieve the needed current range (<10mA - 1500A) the voltage drop across the internal transistor of the INA169 should be around 5V, which will cause serious overheating. Generally, any sensor of the same type we found could not provide us with the desired sensing accuracy and range at the same time. Consequently, we searched for other types of current measuring devices.

Thus, we examined Hall Effect Sensors, and stumbled upon the 2nd option. Then we discovered that the total error of the ACS723 is ±3%, which results in ±150mA. Thus, it cannot be utilized in the design since the requirement for the accuracy is <70mA. Other hall effect sensors displayed similar traits and could not be used for our setup. 

Consequenly, we desided to do further investigation to see if there is any affordable current sensor IC that can measure across the desired range of ±2A with an the required accuracy. First of all we outlined the requirements:

As a result, a decision was made to conduct further investigation aimed at identifying an affordable current sensor IC capable of measuring within the specified range of ±2A with the necessary level of accuracy. 
Initially, the requirements were outlined:


- Bidirectional (±48V min)
- Error <50mA
- Range 70mA - 2A (worst scenario)

### Detected issues

- In high-side sensing mode, we must take into account high voltages, such as 48V (preferably with higher absolute values for common mode voltages, as it is not recommended to operate the ICs at their limit conditions).
- Low-side sensing mode is an option. However, since the shunt resistance cannot be too high(in order to avoid a high current drop) we must still take high voltages into consideration. 
- It also doesn't take much parasitic resistance in wiring or PCB traces to introduce an error into the measured shunt voltage, since the circuit detects a small voltage drop across the shunt resistor. 
> One could solve that problem by using differential sensing(Kelvin sensing) and a difference input amplifier(e.g. INA210)
- The issue with shunt current sensors is that, while they have high accuracy, they cannot provide the sufficient range for our application.
> This could be a viable solution: [50 mA-20 A, Single-Supply, Low-Side or High-Side,Current Sensing Solution](https://www.ti.com/lit/ug/tidu447/tidu447.pdf?ts=1691410071720&ref_url=https%253A%252F%252Fwww.google.com%252F)
- The problem with hall effect sensors is that, while they have a wide range, they lack the accuracy we require.
- Furthermore, most of the shunt sensors I investigated were incapable of handling high magnitude reversed voltages. 
> One could utilize this design in order to overcome this issue: [Low-Cost Bidirectional Current Sensing Using INA181](https://www.ti.com/lit/an/sboa223b/sboa223b.pdf?ts=1691423716311&ref_url=https%253A%252F%252Fwww.google.com%252F)

### Conclusion

This issue proved to be more difficult than we anticipated, requiring analog component selection, PCB design, and simulation to meet the requirements, which requires a lot of time. Thus, we decided to continue testing with the magnetometer, mapping magnetic field measurements to duty cycle while taking into account the current dependency from other channels. Furthermore, upon reviewing of several Helmholtz Cage design reports one could discover that none of them utilized a current sensor. Last but not least, one could mount a magnetometer on the air bearing, eliminating the issue of current feedback loop sensing. This is feasible considering we will almost certainly construct an air bearing model ourselves. This solution will result in more precise measurements (without transfer function error considerations) and lower human resources cost.


Hence, the incorporation of a current sensor was omitted. Instead, we integrated 3 SMA connectors for analog channels on the project's PCB and developed embedded software's that will read analog input from a possible breakout current sensor board developed in the future.


## 5. H-bridges

The H-bridges play a crucial role in this project, enabling the manipulation and inversion of the resultant voltage across the coil terminals, driving the current. This is essential due to the utilization of a 48VDC power supply, necessitating voltage adjustments spanning from -48V to 48V. 



Initially, we investigated the possibility of using the Pololu's MC33926 breakout model. Regrettably, it was soon determined that this model was unsuitable, as the H-Bridge's operational capacity is limited to continuous operation within the range of 5.0V to 28V (with transient operation extended to 40V), as specified in the datasheet available [here](https://www.pololu.com/file/0J233/MC33926.pdf). This misjudgment occurred at a juncture when the power supply had not yet been determined and the refinement of the magnetic field simulation was still in progress.

Subsequently, a decision was reached to assess the viability of employing the COTS Adafruit breakout board, accessible via this [link](https://learn.adafruit.com/adafruit-drv8871-brushed-dc-motor-driver-breakout/). Among the available breakout board H-bridges in Athens, this board offered the highest voltage rating, capable of handling a maximum input power voltage of 45V. Interestingly, the IC used on the board has a rating of up to 50V. Armed with this knowledge, we proceeded to evaluate the board's performance, especially thermal, under a 48V voltage supply.

Thus, while constantly drawing 1.2A, there was no problem with using the IC at 48V (temperatures around 90C). When we pushed it to 1.5A for a sustained period of time, we measured temperatures up to 115C. The following are proposed based on these tests:

* The IC is good for use in this project since
  * Temperatures were below the ICs limits and no shutdown was observed
  * The 1.2A is the worst case scenario regarding current draw, based on calculations.
* A custom PCB will be designed with enhanced thermal dissipation measures
  * More vias under and around the IC
  * Maybe 4 layer
  * Big, solid planes for the power paths
  * Space for coolers and mounting points for fans to be placed above the ICs.


Thus, we proceeded to integrate this IC, available at [Mouser](https://www.ti.com/lit/ds/symlink/drv8871-q1.pdf?HQS=dis-mous-null-mousermode-dsf-pf-null-wwe&ts=1684328419701&ref_url=https%253A%252F%252Fgr.mouser.com%252F), into a custom PCB. During the design process, we took into account the voltage tolerances of each component and ensured proper cooling of the ICs as outlined above. 

Upon further reflection, however, it became apparent that operating electronic components at their limits was not a sound practice and design decision. Initially, our line of thought revolved around the notion that thermal management would be the sole issue at hand. However, upon a more thorough analysis, we recognized that pushing components to their operational extremes could lead to an array of complications beyond just thermal challenges. Components operated at their limits might experience accelerated wear and tear, increased susceptibility to environmental factors, and a heightened probability of failure due to the heightened stress they endure.


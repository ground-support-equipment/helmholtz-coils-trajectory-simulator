# Control box design

The control box consists of the following components:

1. ON/OFF major switch
2. Power Supply/Harnesses hole
3. 6 Banana Female Panael Mount Connectors for coils' ends (3 black / 3 red)
4. 2 Banana Female Panael Mount Connectors for input power (1 black / 1 red)


The diagram below provides specific details regarding the connections and arrangement of the Control Box components.


<p align="center">
  <img src="uploads/CB_diagram.png" width="800" />
</p>


The control box was laser cutted in [hs.gr](https://www.hackerspace.gr/) from acrylic. The final model in shown in the picture below:

The Control Box was fabricated using the laser cutter at [hs.gr](https://www.hackerspace.gr/) from acrylic material. The completed model is displayed in the image below:

<p align="center">
  <img src="uploads/CB-side.jpg" width="400" />
  <img src="uploads/CB-top.jpg" width="400" /> 
</p>

Moreover, the management of harnesses and the final placement of the PCB are illustrated below:

<p align="center">
  <img src="uploads/CB-harnesses.jpg" width="400" />
</p>
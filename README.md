# Helmholtz Coils Hardware

This repository includes all needed source files for 3D printed parts, PCB, wiring and documentation of Helmholtz cage set-up for testing ADCS Cubesat sub-system.

This project was developed in [hackerspace.gr](https://www.hackerspace.gr/) by [Libre Space Foundation](https://libre.space/).

<p align="center">
  <img src="docs/uploads/fdf4bed039795297717e343e14889eb3/coils.png" width="400" height = "310" />
  <img src="docs/uploads/White_Background_mech_setup.png" width="400" height = "310" /> 
</p>

## Documentation

One can access additional information regarding the project's development here: [Hardware Development Documentation](docs/home.md).

## Use

To use the setup available in [hackerspace.gr](https://www.hackerspace.gr/), please consult the documentation that is available [here](https://gitlab.com/librespacefoundation/helmholtz-coils-sw).

## Contribute

The main repository lives on [Gitlab](https://gitlab.com/librespacefoundation/helmholtz-coils-hw) and all Merge Request should happen there.

## License

Licensed under the [CERN OHLv1.2](LICENSE).
